'use strict';

const shell = require('shelljs');
const fs = require('fs');
const path = require('path');
const inquirer = require('inquirer');
const logger = require('./logger');
const remote = require('./remote');
const email = require('./email');
const utils = require('./utils');

const TASKS = {
    CHANGE_VERSION : 'v',
    BUILD          : 'b',
    BACKUP_REMOTE  : 's',
    CLEAN_REMOTE   : 'c',
    DEPLOY_BUILD   : 'd',
    UPLOAD_REPO    : 'u',
    SEND_EMAIL     : 'e'
};

const Web = {
    /**
     * Build web app sources using task manager like, grunt, gulp, webpack, ng, ecc...
     */
    build({ sourcePath, buildSourcesCmd, verbose = false }) {
        process.chdir(sourcePath);
        logger.section(`Compile source:\n$ ${buildSourcesCmd}`);
        shell.exec(buildSourcesCmd, { silent : !verbose });
    },

    /**
     * Set app label version in HTML
     */
    changeVersion({ filePath, version, replacingTag }) {
        logger.section(`Set '${version}' as version in ${filePath}`);
        let versionFile = fs.readFileSync(filePath, 'utf-8');
        this.checkReplacingTag({
            filePath,
            replacingTag,
            checkBuildTask : false
        });
        try {
            let newVersionFile = versionFile.replace(replacingTag, version);
            versionFile = newVersionFile;
            fs.writeFileSync(filePath, versionFile, 'utf-8');
        }
        catch (err) {
            logger.error(err);
        }
    },

    /**
     * Deploy web build to server
     * @param {String} folderSourcePath
     * @param {String} folderDestPath
     * @param {Object} server
     * @param {Boolean} verbose
     */
    deploy({folderSourcePath, folderDestPath, server, verbose}) {
        logger.section(`Deploy web build at ${folderSourcePath} to server ${server.host}:${folderDestPath}`);

        return remote.deploy({folderSourcePath, folderDestPath, server, verbose});
    },

    /**
     * Clean remote dir
     * @param {String} folderPath
     * @param {Object} server
     * @param {Boolean} verbose
     */
    cleanRemote({folderPath, server, verbose}) {
        logger.section(`Clean ${folderPath} on server ${server.host}`);

        return remote.cleanRemote({folderPath, server, verbose});
    },

    /**
     * Backup remote dir
     * @param {String} folderPath
     * @param {Object} server
     * @param {Boolean} verbose
     */
    backupRemote({folderPath, server, verbose}) {
        const d = new Date();
        logger.section(`Backup ${folderPath} on server ${server.host}. Can find this version in ${folderPath}_backup_${d.getFullYear()}${d.getMonth()}${d.getDate()}`);

        return remote.deploy({folderPath, server, verbose});
    },

    /**
     * Upload web build archive to repo
     * @param {*} config
     */
    uploadRepo(config) {
        logger.section(`Upload web build archive to ${config.remote.repo.homepageUrl} repo`);
        return new Promise((resolve, reject) => {
            remote.uploadArchivie({
                archiveFilePath : config.remote.sources.archiveFilePath,
                sourceSrcPath   : path.join(config.buildsDir),
                server          : {
                    host : config.remote.repo.host,
                    port : config.remote.repo.port,
                    user : config.remote.repo.user,
                    pass : config.remote.repo.password
                },
                sourceDestPath : config.remote.repo.buildsPath
            }).then(
                res => {
                    remote.updateRepo({
                        repoPath : config.remote.repo.jsonPath,
                        server   : {
                            host : config.remote.repo.host,
                            port : config.remote.repo.port,
                            user : config.remote.repo.user,
                            pass : config.remote.repo.password
                        },
                        webBuildPath : config.remote.repo.webUrlPath,
                        version      : config.app.versionLabel,
                        changelog    : config.changeLog,
                        releaseDate  : config.releaseDate,
                        hidden       : config.hidden,
                        rootPath     : config.rootPath
                    }).then(resolve, reject);
                },
                reject
            );
        });
    },

    /**
     * Compose email for
     */
    composeEmail({
        appName,
        appLabel,
        appVersion,
        changelog,
        releaseDate,
        repoHomepageUrl,
        webBuildPath = null
    }) {
        let bodyEmail = fs
            .readFileSync(
                path.join(__dirname, '../resources/distribute-web-email.tmpl.html')
            )
            .toString();

        bodyEmail = bodyEmail.replace(/___APP_LABEL___/g, appLabel);
        bodyEmail = bodyEmail.replace(/___APP_NAME___/g, appName);
        bodyEmail = bodyEmail.replace(/___APP_VERSION___/g, appVersion);

        let htmlChangelog = '';
        for (let i = 0; i < changelog.length; i++) {
            htmlChangelog += `✓ ${changelog[i]} <br/>`;
        }

        bodyEmail = bodyEmail.replace(/___CHANGELOG___/g, htmlChangelog);
        bodyEmail = bodyEmail.replace(/___RELEASE_DATE___/g, releaseDate);
        bodyEmail = bodyEmail.replace(/___REPO_HOMEPAGE_URL___/g, repoHomepageUrl);

        const webDirectDownload = webBuildPath;

        if (webDirectDownload) {
            bodyEmail = bodyEmail.replace(
                /___ANGULAR_DIRECT_DOWNLOAD_URL___/g,
                webDirectDownload
            );
            bodyEmail = bodyEmail.replace(
                /___ANDROID_DIRECT_DOWNLOAD_ENCODED_URL___/g,
                encodeURIComponent(webDirectDownload)
            );
        }

        return bodyEmail;
    },

    /**
     * Verify configuration for build and config update steps
     * @param {Object} config
     */
    verifyBuildConfigs(config) {
        if (!config.sources.buildCommand) {
            throw new Error('Source compile error: missing "sources.buildCommand" value in config file');
        }
        if (!fs.existsSync(config.sources.sourcePath)) {
            throw new Error(`Source compile error: directory "sources.sourcePath" doesn't exists at ${config.sources.sourcePath}`
            );
        }
        if (!config.app.version) {
            throw new Error('Invalid build version format: please, see http://semver.org');
        }
    },

    /**
     * Verify configuration for upload build task
     * @param {Object} config
     */
    verifyUploadRepoConfigs(config) {
        if (!config.remote.repo.webUrlPath) {
            throw new Error('Upload repo error: missing "remote.repo.webUrlPath" value in config file');
        }
        if (!config.remote.repo.buildsPath) {
            throw new Error('Upload repo error: missing "remote.repo.buildsPath" value in config file');
        }
    },

    /**
     * Verify configuration for change version steps
     * @param {Object} config
     */
    verifyVersionConfigs(config) {
        if (!config.sources.updateVersion.filePath) {
            throw new Error('Version change error: missing "sources.updateVersion.filePath" value in config file');
        }
        if (!fs.existsSync(config.sources.updateVersion.filePath) && !config.tasks.contains(TASKS.BUILD)) {
            throw new Error(`Version change error: file "sources.updateVersion.filePath" doesn't exists at ${config.sources.updateVersion.filePath}`);
        }
    },

    /**
     * Verify replacingTag at filePath if checkBuildTask not exist
     * @param {Object} filePath
     * @param {Object} replacingTag
     * @param {Object} checkBuildTask
     */
    checkReplacingTag({filePath, replacingTag, checkBuildTask}) {
        let versionFile = fs.readFileSync(filePath, 'utf-8');
        if (versionFile.indexOf(replacingTag) === -1 && !checkBuildTask) {
            throw new Error(`Angular change version error: replacingTag ${replacingTag} not found at file path ${filePath}`);
        }
    },

    /**
     * Verify configuration for change deploy and repo upload step
     * @param {Object} config
     */
    verifyBuildDir(config) {
        if (!config.buildsDir) {
            throw new Error('Error: missing "buildsDir" value in config file');
        }
        if (!fs.existsSync(config.buildsDir) && !config.tasks.contains(TASKS.BUILD)) {
            throw new Error(`Error: folder "buildsDir" doesn't exists at ${config.buildsDir}`);
        }
    },

    /**
     * Inizialize configuration for web build
     */
    initializeSourceBuild(config) {
        return inquirer
            .prompt([
                {
                    type    : 'input',
                    name    : 'buildCommand',
                    message : 'sources.buildCommand',
                    default : 'ng build'
                },
                {
                    type    : 'input',
                    name    : 'sourcePath',
                    message : 'sources.sourcePath',
                    default : './'
                }
            ])
            .then(({ buildCommand, sourcePath }) => {
                if (!config.sources) {
                    config.sources = {};
                }
                config.sources.buildCommand = buildCommand;
                config.sources.sourcePath = sourcePath;
                return config;
            });
    },

    /**
     * Inizialize configuration for change version task
     */
    initializeChangeVersion(config) {
        return inquirer
            .prompt([
                {
                    type    : 'input',
                    name    : 'replacingTag',
                    message : 'sources.updateVersion.replacingTag',
                    default : '{version}'
                },
                {
                    type    : 'input',
                    name    : 'filePath',
                    message : 'sources.updateVersion.filePath',
                    default : 'dist/app_name/index.html'
                }
            ])
            .then(({ replacingTag, filePath }) => {
                if (!config.sources) {
                    config.sources = { updateVersion : {} };
                }
                config.sources.updateVersion.replacingTag = replacingTag;
                config.sources.updateVersion.filePath = filePath;
                return config;
            });
    },

    /**
     * Inizialize builds dir for web command
     */
    initializeBuildsDir(config) {
        return inquirer
            .prompt([
                {
                    type    : 'input',
                    name    : 'buildsDir',
                    message : 'buildsDir',
                    default : 'dist/'
                }
            ])
            .then(({ buildsDir }) => {
                config.buildsDir = buildsDir;
                return config;
            });
    },

    /**
     * Inizialize ditribute.json for web project
     * @param {*} config
     */
    init(config) {
        let web = this;

        const TASKS = {
            CHANGE_VERSION : 'Change app version in file',
            BUILD          : 'Build web app',
            DEPLOY_BUILD   : 'Deploy build to server',
            BACKUP_REMOTE  : 'Backup remote deployed build',
            CLEAN_REMOTE   : 'Clean remote deploy dir',
            UPLOAD_REPO    : 'Upload build on repo server',
            SEND_EMAIL     : 'Send email to working group'
        };

        return inquirer
            .prompt([
                {
                    type    : 'checkbox',
                    message : 'Which tasks you want configure?',
                    name    : 'tasks',
                    choices : [
                        { name : TASKS.CHANGE_VERSION },
                        { name : TASKS.BUILD },
                        { name : TASKS.DEPLOY_BUILD },
                        { name : TASKS.BACKUP_REMOTE },
                        { name : TASKS.CLEAN_REMOTE },
                        { name : TASKS.UPLOAD_REPO },
                        { name : TASKS.SEND_EMAIL }
                    ]
                }
            ])
            .then(({ tasks }) => {
                let questions = [];

                if (tasks.contains(TASKS.CHANGE_VERSION)) {
                    questions.push(web.initializeChangeVersion);
                }

                if (tasks.contains(TASKS.BUILD)) {
                    questions.push(web.initializeSourceBuild);
                }

                if (tasks.contains(TASKS.DEPLOY_BUILD)) {
                    questions.push(web.initializeBuildsDir);
                    questions.push(remote.initializeWebDeploy);
                }

                if (tasks.contains(TASKS.BACKUP_REMOTE)) {
                    questions.push(web.initializeBuildsDir);
                    questions.push(remote.initializeWebDeploy);
                }

                if (tasks.contains(TASKS.CLEAN_REMOTE)) {
                    questions.push(web.initializeBuildsDir);
                    questions.push(remote.initializeWebDeploy);
                }

                if (tasks.contains(TASKS.UPLOAD_REPO)) {
                    questions.push(web.initializeBuildsDir);
                    questions.push(remote.initializeRepoUpdate);
                    questions.push(remote.initializeAngularRepoUpdate);
                }

                if (tasks.contains(TASKS.SEND_EMAIL)) {
                    questions.push(email.initializeSend);
                }

                return utils.askQuestions(questions, config).then(
                    config => {
                        return config;
                    },
                    err => {
                        logger.error(err);
                        process.exit(1);
                    }
                );
            });
    }
};

module.exports = {
    WEB   : Web,
    TASKS : TASKS
};
